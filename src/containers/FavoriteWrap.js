import React from 'react'
import { connect } from 'react-redux'
import { FavoriteContact } from '../actions/contacts.js'
import FavoriteItem from '../components/FavoriteItem.js'

let Favorite = ({ dispatch, contact }) =>  {
  const mark_as_favorite = (id) => {
    dispatch(FavoriteContact(id))
  }
  return (
    <FavoriteItem contact={contact} mark_as_favorite={mark_as_favorite} />
  )
}
Favorite = connect()(Favorite)

export default Favorite