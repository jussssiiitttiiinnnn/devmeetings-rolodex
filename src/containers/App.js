import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import Home from '../components/Home.js'
import ContactPageWrap from '../containers/ContactPageWrap.js'
import 'normalize.css'

export default () => {
  const MenuLink = ({ label, to, activeOnlyWhenExact }) => (
    <Route path={to} exact={activeOnlyWhenExact} children={({ match }) => (
      <Link to={to}  style={match ? {color:'darkturquoise', fontWeight:'bold'} : {color:'deeppink'}}>
        {label}
      </Link>
    )}/>
  )
  return (
    <Router>
      <div className="container">
        <div>
          <MenuLink activeOnlyWhenExact={true} to="/" label=" 🏡 Home"/>
          <MenuLink to="/contact" label=" 😺 Contacts" />
        </div>
        <hr />
        <Route exact path="/" component={Home} />
        <Route path="/contact" component={ContactPageWrap} />
      </div>
    </Router>
  )
}