import React from 'react'
import { connect } from 'react-redux'
import { SearchQuery } from '../actions/search.js'
import Search from '../components/Search.js'

let SearchWrap = ({dispatch, search_query}) => {
  const query = (user_input) => {
    dispatch(SearchQuery(user_input))
  }
  return(
    <Search 
      search={query} 
      query_value={search_query} 
    />
  )
}
SearchWrap = connect(mapStateToProps)(SearchWrap)
export default SearchWrap

function mapStateToProps(state) {
  return {
    search_query: state.search.search_query
  }
}