import React from 'react'

export default ({delete_contact, id}) =>  {
  return(
    <span 
      onClick={ e => {
        delete_contact(id)
      }}
      style={{
        fontSize:'small', 
        color: 'gray',
        cursor:'default'
      }}
    >
    🗑 ✨ 🌈
    </span>
  )
}