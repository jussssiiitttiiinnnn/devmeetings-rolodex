import React from 'react'

export default ({mark_as_favorite, contact}) =>  {
  return(
    <span 
      onClick={ e => {
        e.preventDefault()
        mark_as_favorite(contact.id)
      }}
      style={{cursor:'default'}}
    >
      {contact.favorite ? '❤️' : <span style={{opacity: '0.3'}}>❤️</span> }  
    </span>
  )
}