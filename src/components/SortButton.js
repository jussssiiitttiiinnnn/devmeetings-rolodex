import React from 'react'
import 'milligram'

export default ({sort, order}) =>  {
  const print_order = () => {
    if (order === null){
      return ' 🤷‍♀️ '
    }
    else if (order === true){
      return ' ☝️ '
    }
    else {
      return ' 👇 '
    }
  }
  return(
    <button 
      onClick={ e => {
          e.preventDefault()
          sort()
        }
      }
      className="button button-outline"
    > 
      Sort A-Z {print_order()}
    </button>
  )
}