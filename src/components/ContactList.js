import React from 'react'
import ContactSingle from './ContactSingle.js'

export default ({contacts, show_favorites}) =>  {

  const contact_items = contacts.map((contact, i) => {
    return(
      <ContactSingle
        key={i}
        id={contact.id}
        name={contact.name}
        phone={contact.phone}
        email={contact.email}
        address={contact.address}
        favorite={contact.favorite}
      />
    )
  })
  return(
    <table>
      <thead>
        <tr>
          <td></td>
          <td>😸</td>
          <td>📱</td>
          <td>📞</td>
          <td>🏠</td>
          <td></td>
        </tr>
      </thead>
      <tbody>
        {contact_items}
      </tbody>
    </table>
  )
}