import React from 'react'
import FavoriteWrap from '../containers/FavoriteWrap.js'
import DeleteContactWrap from '../containers/DeleteContactWrap.js'

export default (contact) =>  {
  return(
    <tr>
      <td>
        <FavoriteWrap contact={contact} />
      </td>
      <td>
        <strong>{contact.name}</strong> 
      </td>
      <td>{contact.email}</td>
      <td>{contact.phone}</td>
      <td>{contact.address}</td>
      <td>
        <DeleteContactWrap id={contact.id} />
      </td>
    </tr>
  )
}