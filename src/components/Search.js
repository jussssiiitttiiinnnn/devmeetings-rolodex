import React from 'react'

export default ({query_value, search}) =>  {
  return(
    <form>
      <input 
        style={{
          padding: '12px 15px',
          fontSize: '16px',
          border: '1px solid aquamarine',
        }}
        type="text"
        placeholder="🔎 Search Contacts"
        value={query_value}
        onChange={ e => {
          search(e.target.value)
        }}
      />
    </form>
  )
}